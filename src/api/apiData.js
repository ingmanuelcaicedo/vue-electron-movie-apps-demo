/*  */
import axios from 'axios';

/*  */
const apiKey = '91348baf846811db4a96ec3492cbb1d8';
// var appLanguage = 'en';
const hola = 'hola data';


/******************************************************
 * Get Trending Movies
*******************************************************/
const getTrendingMovies = (category, appLanguage, page = 1) => {
  return axios.get(`https://api.themoviedb.org/3/trending/movie/${category}?api_key=${apiKey}&query=all&page=1&language=${appLanguage}&page=${page}`)
  .then((res) => res 
    // 
  );
}



/******************************************************
 * Get Movie details 
*******************************************************/
const showMovieDetail = (movieId, appLanguage) => {
  return axios.get(`https://api.themoviedb.org/3/movie/${movieId}?api_key=${apiKey}&language=${appLanguage}`) //&language=es
  .then((res) => res 
    // 
  )
}



/******************************************************
 * Get Movie certifications 
*******************************************************/
const getMovieCertifications = (appLanguage) => {
  return axios.get(`https://api.themoviedb.org/3/certification/movie/list?api_key=${apiKey}&language=${appLanguage}`)
  .then((res) => res 
    // 
  )
}



/******************************************************
 * Get Movie certifications 
*******************************************************/
const getTvShowCertifications = (appLanguage) => {
  return axios.get(`https://api.themoviedb.org/3/certification/tv/list?api_key=${apiKey}&language=${appLanguage}`)
  .then((res) => res 
    // 
  )
}



/******************************************************
 * Get TV Shows certifications by County
*******************************************************/
const getTvShowsCertifications = (data) => {
  let res = [];

  if(data) {
    // 
    res = Object.keys(data).map((key) =>  key  ) ;
    //console.log(res);
  }

  return res;
}



/******************************************************
 * Get Movie certifications Countries 
*******************************************************/
const getCertificationsByCountry = (data) => {
  let res = [];

  if(data) {
    // 
    res = Object.keys(data).map((key) =>  key  ) ;
    //console.log(res);
  }

  return res;
}



/******************************************************
 * Get Movie genres
*******************************************************/
const getMovieGenres = (appLanguage) => {
  let res = [];

  return axios.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=${apiKey}&language=${appLanguage}`)
  .then((res) => res
    //
  );
}



/******************************************************
 * Get TvShow genres
*******************************************************/
const getTvShowGenres = (appLanguage) => {
  let res = [];

  return axios.get(`https://api.themoviedb.org/3/genre/tv/list?api_key=${apiKey}&language=${appLanguage}`)
  .then((res) => res
    //
  );
}



export default {
  hola,
  getTrendingMovies,
  showMovieDetail,
  getMovieCertifications,
  getTvShowCertifications,
  getCertificationsByCountry,
  getTvShowsCertifications,
  getMovieGenres,
  getTvShowGenres,
}