import Vue from "vue";
import Router from "vue-router";
//import { component } from "vue/types/umd";

Vue.use(Router);

export default new Router({
    routes: [
      // Home
      {
        path: "/",
        name: "Home",
        component: () => import("@/views/Home"),
        /* children: [
          {
            path: "",
            name: "home",
            component: () => import("@views/Home"),
          }
        ] */
      },
      // Movies
      {
        path: "/movies",
        name: "",
        component: () => import("@/views/movies/HomeMovies"),
        children: [
          {
            path: "",
            name: "TrendingDay",
            component: () => import("@/views/movies/TrendingDay"),
          },
          {
            path: "trending-week",
            name: "TrendingWeek",
            component: () => import("@/views/movies/TrendingWeek"),
          },
          {
            path: "movie-details/:movieId",
            name: "MovieDetails",
            component: () => import("@/views/movies/MovieDetails"),
          }
        ]
      },
      // Companies
      {
        path: "/companies",
        name: "Companies",
        component: () => import("@/views/companies/Companies"),
        children: [
          /* {
            path: "",
            name: "company/:companyId",
            component: () => import("@views/CompanyDetail"),
          } */
        ]
      },
      // Certifications
      {
        path: "/certifications",
        name: "Certifications",
        component: () => import("@/views/certifications/CertificationsHome"),
        children: [
          {
            path: "",
            name: "CertificationsHome",
            component: () => import("@/views/certifications/Certifications"),
          },
          {
            path: "movies",
            name: "MovieCertifications",
            component: () => import("@/views/certifications/MovieCertifications.vue"),
          },
          {
            path: "tv-shows",
            name: "TvShowCertifications",
            component: () => import("@/views/certifications/TvShowCertifications"),
          },
        ]
      },
      // Genres
      {
        path: '/genres',
        name: 'GenresHome',
        component: () => import("@/views/genres/GenresHome.vue"),
        children: [
          {
            path: 'movies',
            name: 'MovieGenres',
            component: () => import("@/views/genres/MovieGenres.vue"),
          },
          {
            path: 'tv-shows',
            name: 'TvShowGenres',
            component: () => import("@/views/genres/TvShowGenres.vue"),
          },
        ],
      },
      /* {
        path: "/",
        component: () => import("@/views/Home"),
        children: [
          {
            path: "",
            name: "home",
            component: () => import("@/views/HomeGlobal")
          },
          {
            path: "my-feed",
            name: "home-my-feed",
            component: () => import("@/views/HomeMyFeed")
          },
          {
            path: "tag/:tag",
            name: "home-tag",
            component: () => import("@/views/HomeTag")
          }
        ]
      }, */
    ]
  }
);