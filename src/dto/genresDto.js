/******************************************************
 * Movie genres dto
*******************************************************/
const movieGenresDto = (data) => {
  let res = [];
  const genres = data.genres;

  if(genres.length > 0) {
    genres.forEach(el => {
        res.push({"name": el.name })
      }
    );
  }

  return res;
};


/******************************************************
 * Movie genres dto
*******************************************************/
const tvShowGenresDto = (data) => {
  let res = [];
  const genres = data.genres;

  if(genres.length > 0) {
    genres.forEach(el => {
        res.push({"name": el.name })
      }
    );
  }

  return res;
};


export default {
  movieGenresDto,
  tvShowGenresDto,
}