//const moviesDto = [];

const movieDto = (data) => {
  return [
    {
      "overview": data.overview,
      "release_date": data.release_date,
      "adult": data.adult,
      "backdrop_path": data.backdrop_path,
      "vote_count": data.vote_count,
      "genre_ids": data.genre_ids,
      "original_title": data.original_title,
      "original_language": data.original_language,
      "id": data.id,
      "poster_path": data.poster_path,
      "title": data.title,
      "video": data.video,
      "vote_average": data.vote_average,
      "popularity": data.popularity,
      "media_type": data.media_type
  }
  ]
}

const movieDetailsDto = (data) => {
  // let res = [];
  let res = {};

  res = //[
    {
      "adult": data.adult,
      "backdrop_path": data.backdrop_path,
      "belongs_to_collection": getBelongsToCollection(data.belongs_to_collection),
      "budget": data.budget,
      "genres": getGenres(data.genres),
      "homepage": data.homepage,
      "id": data.id,
      "imdb_id": data.imdb_id,
      "original_language": data.origin_country,
      "original_title": data.original_title,
      "overview": data.overview,
      "popularity": data.popularity,
      "poster_path": data.poster_path,
      "production_companies": getProductionCompanies(data.production_companies),
      "production_countries": getProductionCountries(data.production_countries),
      "release_date": data.release_date,
      "revenue": data.revenue,
      "runtime": data.runtime,
      "spoken_languages": getSpokenLanguages(data.spoken_languages),
      "status": data.status,
      "tagline": data.tagline,
      "title": data.title,
      "video": data.video,
      "vote_average": data.vote_average,
      "vote_count": data.vote_count,
    }
  //]
  ;
  
  return res;
}



const getBelongsToCollection = (data) => {
  let res = null;
  
  if(data){
    res = {
        "id": data.id,
        "name": data.name,
        "poster_path": data.poster_path,
        "backdrop_path": data.backdrop_path,
    }
  }

  return res;
}

const getGenres = (data) => {
  let res = [];

  if(data.length > 0){
    data.forEach(el => {
      res.push({
        "id": el.id,
        "name": el.name,
      })
    });
  }

  return res;
}

const getProductionCompanies = (data) => {
  let res = [];

  if(data.length > 0){
    data.forEach(el => {
      res.push({
        "id": el.id,
        "logo_path": el.logo_path,
        "name": el.name,
        "origin_country": el.origin_country,
      })
    });
  }
  
  return res;
}

const getProductionCountries = (data) => {
  let res = [];

  if(data.length > 0){
    data.forEach(el => {
      res.push({
        "iso_3166_1": el.iso_3166_1,
        "name": el.name,
        
      })
    });
  }

  return res;
}

const getSpokenLanguages = (data) => {
  let res = []

  if(data.length > 0){
    data.forEach(el => {
      res.push({
        "english_name": el.english_name,
        "iso_639_1": el.iso_639_1,
        "name": el.name,
      })
    });
  }

  return res;
}

export {movieDto, movieDetailsDto};